package azimut;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import azimut.baseTest.Base;
import azimut.data.builder.RegisterSuccessRequestDataBuilder;
import azimut.models.RegisterSuccessRequest;
import azimut.models.RegisterSuccessResponse;
import azimut.models.CreateUserResponse;
import java.util.List;
import java.util.Map;
import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

public class Tests extends Base {

    @Test
    @Tag("integration")
    @DisplayName("Hacer post para crear usuario")
    public void postTest(){
        given()
                .body("{\n" +
                        "    \"email\": \"eve.holt@reqres.in\",\n" +
                        "    \"password\": \"cityslicka\"\n" +
                        "}")
                .post("/login")
                .then()
                .statusCode(HttpStatus.SC_OK) //SC_OK es lo mismo que statusCode(200)
                .body("token", notNullValue());
    }

    @Test
    @Tag("integration")
    @DisplayName("Hacer get para consultar usuarios")
    public void getTest(){
        given()
                .get("/users/2")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("data.id", equalTo(2));
    }

    @Test
    @Tag("integration")
    @DisplayName("Borrar usuario")
    public void deleteTest(){
        given()
                .delete("/users/2")
                .then()
                .statusCode(HttpStatus.SC_NO_CONTENT);
    }

    @Test
    @Tag("integration")
    @DisplayName("Hacer pacth para actualizar usuario")
    public void patchTest(){
        String nameUpdated = given()
                .when()
                .body("{\n" +
                        "    \"name\": \"morpheus_edited\"" +
                        "}")
                .patch("/users/2")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .jsonPath().getString("name");
        assertThat(nameUpdated,equalTo("morpheus_edited"));
    }

    @Test
    @Tag("integration")
    @DisplayName("Hacer put para actualizar usuario")
    public void putTest(){
        String jobUpdated = given()
                .when()
                .body("{\n" +
                        "    \"name\": \"morpheus\",\n" +
                        "    \"job\": \"zion resident\"\n" +
                        "}")
                .put("/users/2")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .jsonPath().getString("job");
        assertThat(jobUpdated,equalTo("zion resident"));
    }

    @Test
    @Tag("integration")
    @DisplayName("Obtener todos los usuarios, obtener headers y body")
    public void getAllUsers(){
        Response response = given()
                .when()
                .get("users?page=2");

        Headers headers = response.getHeaders();
        int statusCode = response.getStatusCode();
        String body = response.getBody().asString();
        String contentType = response.getContentType();

        assertThat(statusCode, equalTo(HttpStatus.SC_OK));
        System.out.println("-----------statusCode-------------");
        System.out.println(statusCode);
        System.out.println("-----------headers-------------");
        System.out.println(headers);
        System.out.println("-----------body-------------");
        System.out.println(body);
        System.out.println("-----------contentType-------------");
        System.out.println(contentType);
        System.out.println("------------------------");
        System.out.println(headers.get("Content-Type"));
        System.out.println(headers.get("Server"));
    }

   @Test
   @Tag("integration")
   @DisplayName("Obtener usuarios con busqueda")
   public void getAllUsersTest(){
        String response = given()
                .when()
                .get("users?page=2")
                .then()
                .extract()
                .body()
                .asString();

       int page = from(response).get("page");
       int totalPages = from(response).get("total_pages");
       int ifFirstUser = from(response).get("data[0].id");
       List<Map> usersWithGreaterThan10 = from(response).get("data.findAll { user -> user.id > 10 }");
       String email = usersWithGreaterThan10.get(0).get("email").toString();
       List<Map> usersRachel = from(response).get("data.findAll { user -> user.first_name = 'Rachel' }");
       String emailRachel = usersRachel.get(0).get("email").toString();

       System.out.println("page = " + page);
       System.out.println("totalPages = " + totalPages);
       System.out.println("ifFirstUser = " + ifFirstUser);
       System.out.println("usersWithGreaterThan10 = " + usersWithGreaterThan10);
       System.out.println("email = " + email);
       System.out.println("usersRachel = " + usersRachel);
       System.out.println("emailRachel = " + emailRachel);
   }

    @Test
    @Tag("integration")
    @DisplayName("Manipular respuesta con POJOs")
    public void createTest(){
        String response = given()
                .when()
                .body("{\n" +
                        "    \"name\": \"morpheus\",\n" +
                        "    \"job\": \"leader\"\n" +
                        "}")
                .post("/users")
                .then()
                .extract()
                .body()
                .asString();
        CreateUserResponse user = from(response).getObject("", CreateUserResponse.class);
        System.out.println(user.getId());
        System.out.println(user.getName());
    }


    @Test
    @Tag("integration")
    @DisplayName("Hacer post mediantes POJOs")
    public void registerSuccessTest() throws JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();
        /*Forma 1.1------
        RegisterSuccessRequest userRequest =
                aUser()
                .withEmail("eve.holt@reqres.in")
                .withPassword("pistol")
                .build();
        System.out.println(userRequest); //este print no funciona para mostrar el contenido o los atributos de userRequest
        System.out.println(objectMapper.writeValueAsString(userRequest));
         */

        /*
        Forma 1.2--------
        RegisterSuccessRequest userRequest = new RegisterSuccessRequest();
        userRequest.setEmail("eve.holt@reqres.in");
        userRequest.setPassword("pistol");
         */

        //Forma 2---------
        /*
        RegisterSuccessRequest userMissingInformation = RegisterSuccessRequestDataFactory.missingAllInformation();
        RegisterSuccessRequest userWithInvalidEmail = RegisterSuccessRequestDataFactory.invalidRegister();
        RegisterSuccessRequest userWithNull = RegisterSuccessRequestDataFactory.nullInformation();
        RegisterSuccessRequest userWithValidInformation = RegisterSuccessRequestDataFactory.validRegister();
         */

        //forma 3----------
        RegisterSuccessRequest userWithAdminRoles = RegisterSuccessRequestDataBuilder
                .anUser()
                .withAdminRoles()
                //.and()
                //.withPassword("socenk34")
                //.but()
                //.withUsernameEmpty()
                .build();
        System.out.println(objectMapper.writeValueAsString(userWithAdminRoles));


        RegisterSuccessResponse userResponse = given()
                .when()
                .body(userWithAdminRoles)
                .post("/register")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .contentType(equalTo("application/json; charset=utf-8"))
                .extract()
                .body()
                .as(RegisterSuccessResponse.class);

        //RegisterSuccessResponse userResponse = from(response).getObject("", RegisterSuccessResponse.class);
        logger.info(userResponse.getToken());
        logger.info(userResponse.getId());

        assertThat(userResponse.getId(),equalTo(4));
        assertThat(userResponse.getToken(),equalTo("QpwL5tke4Pnpja7X4"));


    }
}
