package azimut.data.builder;


import azimut.models.RegisterSuccessRequest;

public class RegisterSuccessRequestBuilder {

    private RegisterSuccessRequest registerSuccessRequest;

    //inicializamos la instancia del objeto que queremos armar
    private RegisterSuccessRequestBuilder(){
        registerSuccessRequest = new RegisterSuccessRequest();//objeto registerSuccessRequest sin valores
    }

    public static RegisterSuccessRequestBuilder aUser(){
        return new RegisterSuccessRequestBuilder();
    }

    public RegisterSuccessRequestBuilder withEmail(String email){ //al objeto registerSuccessRequest le añadimos el email
        this.registerSuccessRequest.setEmail(email);
        return this;
    }

    public RegisterSuccessRequestBuilder withPassword(String password){ //al objeto registerSuccessRequest le añadimos el password
        this.registerSuccessRequest.setPassword(password);
        return this;
    }

    public RegisterSuccessRequest build(){ //objeto registerSuccessRequest con toda la info cargada
        return registerSuccessRequest;
    }
}
