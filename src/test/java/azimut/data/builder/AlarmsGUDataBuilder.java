package azimut.data.builder;

public class AlarmsGUDataBuilder {
    String alarmName = "api-alarm-8";
    String xCurrentOrganization = "55";
    String operator = ">";
    String valueComparation = "10";
    String priority = "LOW";
    String startTime = "00:00:01";
    String endDate = "23:59:59";
    Boolean evaluateInHolidays = true;
    String email = "lmass@azimutenergia.co";
    String groupFunction = "value";
    String typeNotification = "realtime_any_time";


    public String getAlarmName() {
        return alarmName;
    }

    public void setAlarmName(String alarmName) {
        this.alarmName = alarmName;
    }

    public String getxCurrentOrganization() {
        return xCurrentOrganization;
    }

    public void setxCurrentOrganization(String xCurrentOrganization) {
        this.xCurrentOrganization = xCurrentOrganization;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getValueComparation() {
        return valueComparation;
    }

    public void setValueComparation(String valueComparation) {
        this.valueComparation = valueComparation;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Boolean getEvaluateInHolidays() {
        return evaluateInHolidays;
    }

    public void setEvaluateInHolidays(Boolean evaluateInHolidays) {
        this.evaluateInHolidays = evaluateInHolidays;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGroupFunction() {
        return groupFunction;
    }

    public void setGroupFunction(String groupFunction) {
        this.groupFunction = groupFunction;
    }

    public String getTypeNotification() {
        return typeNotification;
    }

    public void setTypeNotification(String typeNotification) {
        this.typeNotification = typeNotification;
    }

    public String createAlarm(String alarmName){

        String query = "{\n" +
                "  \"operationName\": \"CreateAlarm\",\n" +
                "  \"variables\": {\n" +
                "    \"alarm\": {\n" +
                "      \"name\": \"  " + alarmName + " \",\n" +
                "      \"priority\": \""+priority+"\",\n" +
                "      \"schedule\": {\n" +
                "        \"start_time\": \""+startTime+"\",\n" +
                "        \"end_time\": \""+endDate+"\",\n" +
                "        \"week_days\": [\n" +
                "          \"monday\",\n" +
                "          \"tuesday\",\n" +
                "          \"wednesday\",\n" +
                "          \"thursday\",\n" +
                "          \"friday\",\n" +
                "          \"saturday\",\n" +
                "          \"sunday\"\n" +
                "        ],\n" +
                "        \"evaluate_in_holidays\": "+evaluateInHolidays+"\n" +
                "      },\n" +
                "      \"notifications\": {\n" +
                "        \"subscribers\": {\n" +
                "          \"configuration\": {\n" +
                "            \"type\": \"realtime_any_time\"\n" +
                "          },\n" +
                "          \"channels\": {\n" +
                "            \"email\": [\n" +
                "              \"lmass@zimutenergia.co\"\n" +
                "            ]\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      \"rules\": [\n" +
                "        {\n" +
                "          \"id\": \"1\",\n" +
                "          \"equipment\": {\n" +
                "            \"_id\": \"610d9e36ab07ede4d301e6ed\",\n" +
                "            \"name\": \"Test\",\n" +
                "            \"serial\": \"def_test\",\n" +
                "            \"timezone\": \"America/Bogota\",\n" +
                "            \"location_ids\": [\n" +
                "              1571\n" +
                "            ]\n" +
                "          },\n" +
                "          \"event\": {\n" +
                "            \"code\": \"acc_ene\",\n" +
                "            \"i18n\": \"{\\\"es\\\":{\\\"acc_ene\\\":\\\"Energía activa acumulada\\\"}}\",\n" +
                "            \"frequency\": {\n" +
                "              \"type\": \"periodic\",\n" +
                "              \"configuration\": {\n" +
                "                \"value\": 300,\n" +
                "                \"right_offset\": 60,\n" +
                "                \"left_offset\": 60\n" +
                "              }\n" +
                "            },\n" +
                "            \"unit\": {\n" +
                "              \"function\": \"x*0.001\",\n" +
                "              \"source\": \"wh\",\n" +
                "              \"target\": \"kwh\"\n" +
                "            },\n" +
                "            \"type\": \"float\"\n" +
                "          },\n" +
                "          \"frequency\": \"realtime\",\n" +
                "          \"group_function\": \"value\",\n" +
                "          \"operator\": \""+operator+"\",\n" +
                "          \"value\": \""+valueComparation+"\",\n" +
                "          \"data_to_alarm\": {\n" +
                "            \"m\": 1,\n" +
                "            \"n\": 1\n" +
                "          }\n" +
                "        }\n" +
                "      ],\n" +
                "      \"rules_summary\": \"Rule:1\",\n" +
                "      \"service\": {\n" +
                "        \"name\": \"usage-management\",\n" +
                "        \"metadata\": \"{\\\"location\\\":{\\\"_id\\\":1571,\\\"name\\\":\\\"Test_location_ETL\\\"}}\"\n" +
                "      },\n" +
                "      \"organization\": {\n" +
                "        \"_id\": 55,\n" +
                "        \"name\": \"Test_organization\"\n" +
                "      },\n" +
                "      \"seconds_to_close\": 7200\n" +
                "    }\n" +
                "  },\n" +
                "  \"query\": \"mutation CreateAlarm($alarm: AlarmInput!) {\\n  createAlarm(alarm: $alarm) {\\n    _id\\n    name\\n    priority\\n    created_at\\n    status\\n    monitoring_status\\n    __typename\\n  }\\n}\\n\"\n" +
                "}";
        return query;
    }

}


