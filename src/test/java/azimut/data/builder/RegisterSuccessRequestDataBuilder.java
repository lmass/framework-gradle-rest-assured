package azimut.data.builder;

import org.apache.commons.lang3.StringUtils;
import azimut.data.factory.RegisterSuccessRequestDataFactory;
import azimut.models.RegisterSuccessRequest;

public class RegisterSuccessRequestDataBuilder {

    private RegisterSuccessRequest registerSuccessRequest;

    public static RegisterSuccessRequestDataBuilder anUser(){
        return new RegisterSuccessRequestDataBuilder();
    }

    private RegisterSuccessRequestDataBuilder(){
        createDefaultRegister();
    }

    private void createDefaultRegister(){
        registerSuccessRequest = new RegisterSuccessRequest();
        this.registerSuccessRequest = RegisterSuccessRequestDataFactory.validRegister();
    }

    public RegisterSuccessRequestDataBuilder withUsername(String username){
        this.registerSuccessRequest.setEmail(username);
        return this;
    }

    public RegisterSuccessRequestDataBuilder withUsernameEmpty(){
        this.registerSuccessRequest.setEmail(StringUtils.EMPTY);
        return this;
    }

    public RegisterSuccessRequestDataBuilder withPassword(String password){
        this.registerSuccessRequest.setPassword(password);
        return this;
    }

    public RegisterSuccessRequestDataBuilder withPasswordEmpty(){
        this.registerSuccessRequest.setPassword(StringUtils.EMPTY);
        return this;
    }

    public RegisterSuccessRequestDataBuilder withRol(String role){
        this.registerSuccessRequest.setRole(role);
        return this;
    }

    public RegisterSuccessRequestDataBuilder withAdminRoles(){
        this.registerSuccessRequest.setRole("Admin");
        return this;
    }

    public RegisterSuccessRequestDataBuilder but(){  //este método no hace nada, es solo de lectura
        return this;
    }

    public RegisterSuccessRequestDataBuilder and(){  //este método no hace nada, es solo de lectura
        return this;
    }

    public RegisterSuccessRequest build(){
        return registerSuccessRequest;
    }

}
