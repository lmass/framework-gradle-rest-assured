package azimut.data.builder.Locations;

public class LocationsCreationsDataBuilder {
    String xCurrentOrganization = "55";
    String parenId = "null";
    String description = "null";
    Boolean layerRealty = false;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getLayerRealty() {
        return layerRealty;
    }

    public void setLayerRealty(Boolean layerRealty) {
        this.layerRealty = layerRealty;
    }



    public String getxCurrentOrganization() {
        return xCurrentOrganization;
    }

    public void setxCurrentOrganization(String xCurrentOrganization) {
        this.xCurrentOrganization = xCurrentOrganization;
    }

    public String getParenId() {
        return parenId;
    }

    public void setParenId(String parenId) {
        this.parenId = parenId;
    }

    public String createLocation(String nameLocation){

        if(layerRealty){
           return  "{\n" +
                   "  \"operationName\": \"create\",\n" +
                   "  \"variables\": {\n" +
                   "    \"location\": {\n" +
                   "      \"layers\": {\n" +
                   "        \"realty\": {\n" +
                   "          \"country\": {\n" +
                   "            \"name\": \"Colombia\",\n" +
                   "            \"_id\": \"CO\"\n" +
                   "          }\n" +
                   "        }\n" +
                   "      },\n" +
                   "      \"organization_id\": "+xCurrentOrganization+",\n" +
                   "      \"parent_id\": "+parenId+",\n" +
                   "      \"description\": "+description+",\n" +
                   "      \"name\": \""+nameLocation+"\"\n" +
                   "    }\n" +
                   "  },\n" +
                   "  \"query\": \"mutation create($location: LocationInput!) {\\n  createLocation(location: $location) {\\n    _id\\n    created_at\\n    description\\n    name\\n    organization_id\\n    parent_id\\n    updated_at\\n    layers {\\n      company {\\n        ...all_company_layer\\n        __typename\\n      }\\n      realty {\\n        ...all_realty_layer\\n        __typename\\n      }\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n\\nfragment all_company_layer on CompanyLayer {\\n  capacity\\n  ciiu\\n  class\\n  production\\n  schedule {\\n    ...all_period\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment all_period on Period {\\n  days\\n  end_time\\n  start_time\\n  __typename\\n}\\n\\nfragment all_realty_layer on RealtyLayer {\\n  address\\n  area\\n  image\\n  oneline_diagram\\n  country {\\n    ...all_admin_division\\n    __typename\\n  }\\n  region {\\n    ...all_admin_division\\n    __typename\\n  }\\n  city {\\n    ...all_admin_division\\n    __typename\\n  }\\n  geolocation {\\n    ...all_geolocation\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment all_admin_division on AdministrativeDivision {\\n  _id\\n  name\\n  __typename\\n}\\n\\nfragment all_geolocation on Geolocation {\\n  lat\\n  lng\\n  __typename\\n}\\n\"\n" +
                   "}";
        } else {
            return "{\n" +
                    "  \"operationName\": \"create\",\n" +
                    "  \"variables\": {\n" +
                    "    \"location\": {\n" +
                    "      \"organization_id\": "+xCurrentOrganization+",\n" +
                    "      \"parent_id\": "+parenId+",\n" +
                    "      \"description\": "+description+",\n" +
                    "      \"name\": \""+nameLocation+"\"\n" +
                    "    }\n" +
                    "  },\n" +
                    "  \"query\": \"mutation create($location: LocationInput!) {\\n  createLocation(location: $location) {\\n    _id\\n    created_at\\n    description\\n    name\\n    organization_id\\n    parent_id\\n    updated_at\\n    layers {\\n      company {\\n        ...all_company_layer\\n        __typename\\n      }\\n      realty {\\n        ...all_realty_layer\\n        __typename\\n      }\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n\\nfragment all_company_layer on CompanyLayer {\\n  capacity\\n  ciiu\\n  class\\n  production\\n  schedule {\\n    ...all_period\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment all_period on Period {\\n  days\\n  end_time\\n  start_time\\n  __typename\\n}\\n\\nfragment all_realty_layer on RealtyLayer {\\n  address\\n  area\\n  image\\n  oneline_diagram\\n  country {\\n    ...all_admin_division\\n    __typename\\n  }\\n  region {\\n    ...all_admin_division\\n    __typename\\n  }\\n  city {\\n    ...all_admin_division\\n    __typename\\n  }\\n  geolocation {\\n    ...all_geolocation\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment all_admin_division on AdministrativeDivision {\\n  _id\\n  name\\n  __typename\\n}\\n\\nfragment all_geolocation on Geolocation {\\n  lat\\n  lng\\n  __typename\\n}\\n\"\n" +
                    "}";
        }


/*
        String queryWithoutParent = "{\n" +
                "  \"operationName\": \"create\",\n" +
                "  \"variables\": {\n" +
                "    \"location\": {\n" +
                "      \"organization_id\": 55,\n" +
                "      \"parent_id\": null,\n" +
                "      \"description\": null,\n" +
                "      \"name\": \"api - creación locación sin padre y sin inmueble\"\n" +
                "    }\n" +
                "  },\n" +
                "  \"query\": \"mutation create($location: LocationInput!) {\\n  createLocation(location: $location) {\\n    _id\\n    created_at\\n    description\\n    name\\n    organization_id\\n    parent_id\\n    updated_at\\n    layers {\\n      company {\\n        ...all_company_layer\\n        __typename\\n      }\\n      realty {\\n        ...all_realty_layer\\n        __typename\\n      }\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n\\nfragment all_company_layer on CompanyLayer {\\n  capacity\\n  ciiu\\n  class\\n  production\\n  schedule {\\n    ...all_period\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment all_period on Period {\\n  days\\n  end_time\\n  start_time\\n  __typename\\n}\\n\\nfragment all_realty_layer on RealtyLayer {\\n  address\\n  area\\n  image\\n  oneline_diagram\\n  country {\\n    ...all_admin_division\\n    __typename\\n  }\\n  region {\\n    ...all_admin_division\\n    __typename\\n  }\\n  city {\\n    ...all_admin_division\\n    __typename\\n  }\\n  geolocation {\\n    ...all_geolocation\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment all_admin_division on AdministrativeDivision {\\n  _id\\n  name\\n  __typename\\n}\\n\\nfragment all_geolocation on Geolocation {\\n  lat\\n  lng\\n  __typename\\n}\\n\"\n" +
                "}";

 */
    }

}
