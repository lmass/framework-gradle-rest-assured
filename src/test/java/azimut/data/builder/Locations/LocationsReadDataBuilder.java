package azimut.data.builder.Locations;

public class LocationsReadDataBuilder {
    String xCurrentOrganization = "55";

    public String getxCurrentOrganization() {
        return xCurrentOrganization;
    }

    public void setxCurrentOrganization(String xCurrentOrganization) {
        this.xCurrentOrganization = xCurrentOrganization;
    }

    public String readLocation() {
        return "{\n" +
                "  \"operationName\": \"index\",\n" +
                "  \"variables\": {\n" +
                "    \"withDates\": true,\n" +
                "    \"organization_id\": " + xCurrentOrganization + "\n" +
                "  },\n" +
                "  \"query\": \"query index($organization_id: Int, $parent_id: Int, $withDates: Boolean!) {\\n  locations(organization_id: $organization_id, parent_id: $parent_id) {\\n    _id\\n    created_at @include(if: $withDates)\\n    description\\n    name\\n    organization_id\\n    parent_id\\n    updated_at @include(if: $withDates)\\n    layers {\\n      company {\\n        ...all_company_layer\\n        __typename\\n      }\\n      realty {\\n        ...all_realty_layer\\n        __typename\\n      }\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n\\nfragment all_company_layer on CompanyLayer {\\n  capacity\\n  ciiu\\n  class\\n  production\\n  schedule {\\n    ...all_period\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment all_period on Period {\\n  days\\n  end_time\\n  start_time\\n  __typename\\n}\\n\\nfragment all_realty_layer on RealtyLayer {\\n  address\\n  area\\n  image\\n  oneline_diagram\\n  country {\\n    ...all_admin_division\\n    __typename\\n  }\\n  region {\\n    ...all_admin_division\\n    __typename\\n  }\\n  city {\\n    ...all_admin_division\\n    __typename\\n  }\\n  geolocation {\\n    ...all_geolocation\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment all_admin_division on AdministrativeDivision {\\n  _id\\n  name\\n  __typename\\n}\\n\\nfragment all_geolocation on Geolocation {\\n  lat\\n  lng\\n  __typename\\n}\\n\"\n" +
                "}";
    }

}


