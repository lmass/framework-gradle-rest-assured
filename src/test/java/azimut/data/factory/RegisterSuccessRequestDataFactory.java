package azimut.data.factory;

import com.github.javafaker.Faker;
import org.apache.commons.lang3.StringUtils;
import azimut.models.RegisterSuccessRequest;
import static azimut.data.builder.RegisterSuccessRequestBuilder.aUser;

public class RegisterSuccessRequestDataFactory {

    private static final Faker faker = new Faker();
    private static final String DEFAULT_USERNAME = "eve.holt@reqres.in";


    public static RegisterSuccessRequest missingAllInformation(){  //objeto RegisterSuccessRequest con info vacía
        return aUser()
                .withPassword(StringUtils.EMPTY)
                .withEmail(StringUtils.EMPTY)
                .build();
    }

    public static RegisterSuccessRequest nullInformation(){  //objeto RegisterSuccessRequest con info vacía
        return aUser()
                .withPassword(null)
                .withEmail(null)
                .build();
    }

    public static RegisterSuccessRequest validRegister(){  //objeto RegisterSuccessRequest con info vacía
        return aUser()
                .withPassword(faker.internet().password())
                .withEmail(DEFAULT_USERNAME) // por defecto hay que ponerle ese email para la API que estamos probando
                .build();
    }

    public static RegisterSuccessRequest invalidRegister(){  //objeto RegisterSuccessRequest con info vacía
        return aUser()
                .withPassword(faker.internet().password())
                .withEmail(faker.internet().emailAddress())
                .build();
    }

}
