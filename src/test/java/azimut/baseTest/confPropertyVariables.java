package azimut.baseTest;

import java.util.Optional;

public class confPropertyVariables {

    public static String getHost(){
        return Optional.ofNullable(System.getenv("host"))
                .orElse((String) EnvProperties.getInstance().get("host"));
    }

    public static String getPath(){
        return Optional.ofNullable(System.getenv("path"))
                .orElse((String) EnvProperties.getInstance().get("path"));
    }

}
