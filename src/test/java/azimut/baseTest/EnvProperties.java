package azimut.baseTest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Optional;
import java.util.Properties;

import static org.apache.logging.log4j.core.util.Loader.getClassLoader;

public class EnvProperties {

    public static final String ENV_PREFIX = "env";
    public static final String ENV_SUFFIX = "properties";
    public static final Logger logger = LogManager.getLogger(EnvProperties.class);
    private static Properties instance = null;

    public static synchronized Properties getInstance(){
        if(instance == null){ //la instancia no se ha creado
            instance = loadPropertiesFile();
        } //si no está nulo (se tuvo que haber creado ya, devuelvame la instancia)
        return instance;
    }

    private EnvProperties(){

    }
    private static Properties loadPropertiesFile(){
        String environment = Optional.ofNullable(System.getenv("env"))
                .orElse("prod");

        String fileName = String.format("%s-%s.%s", ENV_PREFIX, environment, ENV_SUFFIX);
        logger.info("Property file to read {}", fileName);
        Properties prop = new Properties();
        try {
            prop.load(getClassLoader().getResourceAsStream(fileName));
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("Unable to load the file {}", fileName);
        }
        return prop;
    }

}
