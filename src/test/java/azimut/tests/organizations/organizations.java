package azimut.tests.organizations;

import io.qameta.allure.*;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import azimut.baseTest.Base;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isEmptyOrNullString;

public class organizations extends Base{
    String xApiKey = "da2-4mmrdfmqvjbm3gzsmz2qfrq2f4";
    String baseUri = "https://api.organizations.azimutenergia-test.co/";


    @Test
    @Tag("organizations")
    @Feature("Organizaciones")
    @Story("Usuario intenta acceder a la información de su propia organización")
    @DisplayName("Usuario user_test de la organización 54 (Tigo) logra acceder a dicha organización")
    @Severity(SeverityLevel.CRITICAL)
    void getOrganizationsTigo() {
        String xCurrentOrganization = "54";
        String query = "{\"operationName\":\"index\",\"variables\":{\"ids\":[" + xCurrentOrganization + "],\"withDates\":true},\"query\":\"query index($withDates: Boolean!, $ids: [Int!]) {\\n  organizations(ids: $ids) {\\n    _id\\n    created_at @include(if: $withDates)\\n    description\\n    name\\n    updated_at @include(if: $withDates)\\n    __typename\\n  }\\n}\\n\"}";

        given()
                .when()
                .header("x-current-organization", xCurrentOrganization)
                .header("x-api-key", xApiKey)
                .baseUri(baseUri)
                .body(query)
                .post("/graphql")
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("data.organizations._id[0]", equalTo(54))
                .body("data.organizations.name[0]", equalTo("Tigo"))
                .body(matchesJsonSchemaInClasspath("schemas/organizations.json"));
    }


    @Test
    @Tag("organizations")
    @Feature("Organizaciones")
    @Story("Usuario intenta acceder a la información de otra organización organización")
    @DisplayName("Usuario user_test de la organización 54 (Tigo) NO logra acceder a la información de la organización 1(PGS)")
    @Severity(SeverityLevel.CRITICAL)
    void getOrganization1() {
        String xCurrentOrganization = "1";
        String query = "{\"operationName\":\"index\",\"variables\":{\"ids\":[" + xCurrentOrganization + "],\"withDates\":true},\"query\":\"query index($withDates: Boolean!, $ids: [Int!]) {\\n  organizations(ids: $ids) {\\n    _id\\n    created_at @include(if: $withDates)\\n    description\\n    name\\n    updated_at @include(if: $withDates)\\n    __typename\\n  }\\n}\\n\"}";

        given()
                .when()
                .header("x-current-organization", xCurrentOrganization)
                .header("x-api-key", xApiKey)
                .baseUri(baseUri)
                .body(query)
                .post("/graphql")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("data.organizations[0]", isEmptyOrNullString())
                .body("data.organizations.name[0]", isEmptyOrNullString())
                .body("data.organizations._id[0]", isEmptyOrNullString())
                .body("data.organizations.__typename[0]", isEmptyOrNullString());
    }


    @Test
    @Tag("organizations")
    @Feature("Organizaciones")
    @Story("Usuario intenta acceder a la información de otra organización organización")
    @DisplayName("Usuario user_test de la organización 54 (Tigo) NO logra acceder a la información de la organización 5 (Universidad EIA)")
    @Severity(SeverityLevel.CRITICAL)
    void getOrganization5() {
        String xCurrentOrganization = "6";
        String query = "{\"operationName\":\"index\",\"variables\":{\"ids\":[" + xCurrentOrganization + "],\"withDates\":true},\"query\":\"query index($withDates: Boolean!, $ids: [Int!]) {\\n  organizations(ids: $ids) {\\n    _id\\n    created_at @include(if: $withDates)\\n    description\\n    name\\n    updated_at @include(if: $withDates)\\n    __typename\\n  }\\n}\\n\"}";

        given()
                .when()
                .header("x-current-organization", xCurrentOrganization)
                .header("x-api-key", xApiKey)
                .baseUri(baseUri)
                .body(query)
                .post("/graphql")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("data.organizations[0]", isEmptyOrNullString());
    }


    @Test
    @Feature("Organizaciones")
    @Story("Usuario intenta acceder a la información de otra organización organización")
    @DisplayName("Usuario user_test de la organización 54 (Tigo) NO logra acceder a la información de otras organizaciones")
    @Severity(SeverityLevel.CRITICAL)
    void getAllOrganizations() {
        for(int i=1;i<=5;i++) {
            String xCurrentOrganization = String.valueOf(i);
            String query = "{\"operationName\":\"index\",\"variables\":{\"ids\":[" + xCurrentOrganization + "],\"withDates\":true},\"query\":\"query index($withDates: Boolean!, $ids: [Int!]) {\\n  organizations(ids: $ids) {\\n    _id\\n    created_at @include(if: $withDates)\\n    description\\n    name\\n    updated_at @include(if: $withDates)\\n    __typename\\n  }\\n}\\n\"}";
            try {
                given()
                        .when()
                        .header("x-current-organization", xCurrentOrganization)
                        .header("x-api-key", xApiKey)
                        .baseUri(baseUri)
                        .body(query)
                        .post("/graphql")
                        .then()
                        .assertThat()
                        .statusCode(HttpStatus.SC_OK)
                        .body("data.organizations[0]", isEmptyOrNullString());
            } catch (AssertionError ex) {
                logger.info("error con la organización" + xCurrentOrganization);
            }
        }
    }
}
