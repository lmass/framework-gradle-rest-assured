package azimut.tests.organizations;

import azimut.baseTest.Base;
import azimut.data.builder.Locations.LocationsCreationsDataBuilder;
import azimut.data.builder.Locations.LocationsReadDataBuilder;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;
import static org.hamcrest.Matchers.*;

public class locations extends Base {

    String xApiKey = "da2-4mmrdfmqvjbm3gzsmz2qfrq2f4";
    String baseUri = "https://api.organizations.azimutenergia-test.co/";
    LocationsReadDataBuilder location = new LocationsReadDataBuilder();
    LocationsCreationsDataBuilder createLocation = new LocationsCreationsDataBuilder();

    @Test
    @Tag("locations")
    @Feature("Servicio de locaciones")
    @Story("Usuario lee los locations de la organización")
    @DisplayName("Usuario con permisos full access intenta leer los locations de las organizaciones")
    @Severity(SeverityLevel.CRITICAL)
    public void readAllLocations(){
        String xCurrentOrganization = "25";

        location.setxCurrentOrganization(xCurrentOrganization);
        String query = location.readLocation();

        given()
                .when()
                .header("x-current-organization", xCurrentOrganization)
                .header("x-api-key", xApiKey)
                .baseUri(baseUri)
                .body(query)
                .post("/graphql")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("data.locations[0]", notNullValue());
    }

    @Test
    @Tag("locations")
    @Feature("Servicio de locaciones")
    @Story("Usuario crea un location de su misma organización")
    @DisplayName("Usuario con permisos full access intenta crear un location de su misma organización")
    @Severity(SeverityLevel.CRITICAL)
    public void createLocation(){
        String xCurrentOrganization = "55";
        String nameLocation = "Location prueba rest assured 3";

        createLocation.setxCurrentOrganization(xCurrentOrganization);
        //createLocation.setParenId("1613");
        createLocation.setLayerRealty(false);

        String query = createLocation.createLocation(nameLocation);

        given()
                .when()
                .header("x-current-organization", xCurrentOrganization)
                .header("x-api-key", xApiKey)
                .baseUri(baseUri)
                .body(query)
                .post("/graphql")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("data.createLocation.name", equalTo(nameLocation));
    }
}
