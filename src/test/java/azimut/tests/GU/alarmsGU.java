package azimut.tests.GU;

import azimut.baseTest.Base;
import azimut.data.builder.AlarmsGUDataBuilder;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.equalTo;

public class alarmsGU extends Base {

    String xApiKey = "da2-nplqwelg3nccfaan3vbcvde7sy";
    String baseUri = "https://api.alarms.azimutenergia-test.co/";

    AlarmsGUDataBuilder alarmaGU = new AlarmsGUDataBuilder();


    @Test
    @Feature("Alarms")
    @Story("Creacion de alarma")
    @DisplayName("Creación de alarma evento Integer")
    @Severity(SeverityLevel.CRITICAL)
    void getAlarms() {
        String xCurrentOrganization = "55";
        String query = "{\"operationName\":\"GetPaginatedAlarms\",\"variables\":{\"limit\":10,\"skip\":0,\"service\":\"usage-management\"},\"query\":\"query GetPaginatedAlarms($limit: Int!, $skip: Int!, $service: String!) {\\n  alarms(pagination: {limit: $limit, skip: $skip}, filters: {service: $service}) {\\n    total\\n    data {\\n      _id\\n      name\\n      priority\\n      created_at\\n      status\\n      monitoring_status\\n      service {\\n        metadata\\n        __typename\\n      }\\n      rules {\\n        event {\\n          i18n\\n          code\\n          frequency {\\n            configuration {\\n              value\\n              right_offset\\n              left_offset\\n              __typename\\n            }\\n            type\\n            __typename\\n          }\\n          type\\n          unit {\\n            function\\n            source\\n            target\\n            __typename\\n          }\\n          __typename\\n        }\\n        equipment {\\n          _id\\n          location_ids\\n          name\\n          serial\\n          timezone\\n          __typename\\n        }\\n        data_to_alarm {\\n          m\\n          n\\n          __typename\\n        }\\n        frequency\\n        group_function\\n        id\\n        operator\\n        value\\n        __typename\\n      }\\n      rules_summary\\n      schedule {\\n        end_time\\n        evaluate_in_holidays\\n        start_time\\n        week_days\\n        __typename\\n      }\\n      notifications {\\n        subscribers {\\n          channels {\\n            email\\n            sms\\n            telegram\\n            whatsapp\\n            __typename\\n          }\\n          configuration {\\n            conditions {\\n              period\\n              times\\n              __typename\\n            }\\n            type\\n            __typename\\n          }\\n          __typename\\n        }\\n        __typename\\n      }\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n\"}";
        given()
                .when()
                .auth().basic("lmass","Test_2021#")
                .header("x-current-organization", xCurrentOrganization)
                .header("x-api-key", xApiKey)
                .baseUri(baseUri)
                .body(query)
                .post("/graphql")
                .then()
                .statusCode(HttpStatus.SC_OK);
    }


    @Test
    @Tag("api-alarms-gu")
    void createAlarm1(){
        String xCurrentOrganization = "55";

        String AlarmName = "GU: alarm pipeline";
        alarmaGU.setPriority("LOW");
        alarmaGU.setStartTime("00:00:00");
        alarmaGU.setEndDate("23:59:59");
        alarmaGU.setEvaluateInHolidays(true);
        alarmaGU.setEmail("lmass24@hotmail.com");
        alarmaGU.setGroupFunction("value");
        alarmaGU.setOperator(">");
        alarmaGU.setValueComparation("1000");


        String query = alarmaGU.createAlarm(AlarmName);

        given()
                .when()
                .header("x-current-organization", xCurrentOrganization)
                .header("x-api-key", xApiKey)
                .contentType(ContentType.JSON)
                .baseUri(baseUri)
                .body(query)
                .post("/graphql")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .assertThat()
                .body(matchesJsonSchemaInClasspath("schemas/alarms-gu.json"))
                .body("data.createAlarm.name", equalTo(AlarmName));
    }
}