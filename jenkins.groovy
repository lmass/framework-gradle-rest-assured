pipeline{
    agent any
    stages
            {
                stage('Download code')
                        {
                            steps
                                    {
                                        git 'https://gitlab.com/lmass/framework-gradle-rest-assured.git'
                                    }
                        }
                stage('Run Tests')
                        {
                            steps{
                                withGradle{
                                    sh './gradlew clean test'
                                }
                            }
                        }
            }
    post {
        always{
            script{
                allure([
                        includeProperties: false,
                        jdk              : '',
                        properties       : [],
                        reportBuildPolicy: 'ALWAYS',
                        results          : [[path: 'build/allure-results']]
                ])
            }
        }
    }

}